package moteur;


/** 
 * Classe <b>abstraite</b> décrivant une entité (objet présent sur le plateau).
 * 
 * @author Evan
 * @version b03
 *
 */
public abstract class Entite {
	
	public int x;
	public int y;
	public char symbole;
	public char marqueur;
	public char caseExplore;
	
	/**
	 * Place une entité au hasard sur une hitbox donné.
	 * @param hitbox La hitbox de réference
	 * @param symbole Le symbole associé à l'entité
	 */
	public Entite(char[][] hitbox, char symbole, char marqueur, int x, int y) {
		this.symbole = symbole;
		this.x = x;
		this.y = y;
		hitbox[this.x][this.y] = this.symbole;
		this.marqueur = marqueur;
		this.caseExplore = Plateau.getCaseVide();
	}
	
	/**
	 * Méthode vérifiant la possibilité de déplacer une entité
	 * TODO: Gestion de la superposition d'entités. 
	 * @param s Le sens de déplacement prévu
	 * @param hitbox La hitbox du jeu
	 * @return Un booléen vrai si et seulement si l'entité peut se déplacer.
	 */
	public boolean estDeplacable(Sens s, char[][] hitbox) {
		int sensPrevuX = this.x + s.getX();
		int sensPrevuY = this.y + s.getY();
		return !((sensPrevuX < 0) || (sensPrevuY < 0)
				|| (sensPrevuX >= hitbox.length)
				|| (sensPrevuY >= hitbox[0].length));
	}

	/**
	 * Méthode vérifiant une éventuelle collision entre le Chasseur et le Monstre, ce qui déclancherait la victoire du Chasseur.
	 * @param s Le sens de déplacement prévu
	 * @param hitbox La hitbox du jeu
	 * @return Un booléen vrai si il y aura collision.
	 */
	public boolean estEnCollision(Sens s, char[][] hitbox) {
		int sensPrevuX = this.x + s.getX();
		int sensPrevuY = this.y + s.getY();
		return (hitbox[sensPrevuX][sensPrevuY] == Monstre.symbole || hitbox[sensPrevuX][sensPrevuY] == Chasseur.symbole);
	}
	
	/**
	 * Méthode générique pour toute entité n'étant pas un Monstre.
	 * @return true
	 */
	public boolean estVisitee(){
		return true;
	}
	
	/** 
	 * Assure le déplacement effectif de l'entité dans la hitbox.
	 * @param s Le sens de déplacement prévu
	 * @param hitbox Une hitbox
	 */
	public void deplacement(Sens s, char[][] hitbox) {
		if(this.caseExplore == Plateau.getCaseVide() && this.marqueur != '0') {
			hitbox[this.x][this.y] = this.marqueur;
		}else {
			hitbox[this.x][this.y] = this.caseExplore;
		}
		this.x += s.getX();
		this.y += s.getY();
		this.caseExplore = hitbox[this.x][this.y];
		hitbox[this.x][this.y] = this.symbole;
	}
	
}
