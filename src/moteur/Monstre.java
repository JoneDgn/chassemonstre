package moteur;

/**
 * Extension de Entite pour les spécificités du Monstre.
 * @author Evan
 * @version b01
 * 
 */
public class Monstre extends Entite{

	public static char symbole = 'M';
	public int casesVisitees = 1;

	/**
	 * Place un Monstre au hasard sur une hitbox donné.
	 * @param hitbox La hitbox de réference
	 * @param xy Une coordonée
	 */
	public Monstre(char[][] hitbox, int[] xy) {
		super(hitbox, symbole, 'x', xy[0], xy[1]);
	}
	
	/**
	 * Détermine si un monstre à déja visité la case sur laquelle est positionné l'instance courante.
	 */
	public boolean estVisitee() {
		return this.caseExplore != 'x';
	}
}
