package moteur;

import java.util.Random;

/**
 * Classe définissant un couple de coordonnées aléatoires differentes.
 * 
 * @author evan
 * @version b01
 *
 */
public class CoupleAleatoire {

	private int[] a;
	private int[] b;
	
	/**
	 * Constructeur qui s'occupe de la génération des coordonnées.
	 * @param hitbox
	 */
	public CoupleAleatoire(char[][] hitbox) {
		this.a = new int[2];
		this.b = new int[2];
		Random rng = new Random();
		a[0] = rng.nextInt(hitbox.length);
		a[1] = rng.nextInt(hitbox[0].length);
		b[0] = a[0];
		b[1] = a[1];
		while(b[0] == a[0])b[0] = rng.nextInt(hitbox.length);
		while(b[1] == a[1])b[1] = rng.nextInt(hitbox[0].length);
	}
	
	/**
	 * @return Revoie la coordonnée aléatoire A.
	 */
	public int[] getA() {
		return this.a;
	}
	
	/**
	 * @return Revoie la coordonnée aléatoire B.
	 */
	public int[] getB() {
		return this.b;
	}
}
