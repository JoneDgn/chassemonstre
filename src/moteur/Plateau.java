package moteur;

/**
 * Classe du plateau.
 *  
 * Un plateau est défini par sa hitbox, un chasseur et un monstre et un couple de nombres aléatoires.
 * @author Evan, Yohan
 * @version b04
 *
 */
public class Plateau {

	private char[][]hitbox;
	private int taille;
	public Chasseur chasseur;
	public Monstre monstre;
	public CoupleAleatoire coordonneesInitiales;
	private final int LONGUEUR;
	private final int LARGEUR;
	
	private static final char CASE_VIDE = '.';

	/**
	 * Constructeur du plateau.
	 * Un plateau contient un chasseur et un monstre placé aléatoirement sur une hitbox.
	 * @param longueur La longueur du plateau
	 * @param largeur La largeur du plateau
	 */
	public Plateau(int longueur, int largeur){
		this.LONGUEUR = longueur;
		this.LARGEUR = largeur;
		this.hitbox = new char[longueur][largeur];
		this.taille = longueur * largeur;
		for(int i=0; i<hitbox.length; i++) {
			for(int j=0; j<hitbox[i].length; j++) {
				hitbox[i][j] = Plateau.CASE_VIDE;
			}
		}
		this.coordonneesInitiales = new CoupleAleatoire(this.hitbox);
		this.chasseur = new Chasseur(this.hitbox, coordonneesInitiales.getA());
		this.monstre = new Monstre(this.hitbox, coordonneesInitiales.getB());
	}
	
	/**
	 * Méthode d'affichage
	 * @return Une représentation visuelle du plateau.
	 */
	public String getMap() {
		StringBuilder map = new StringBuilder();
		for(int i=0; i<hitbox[0].length; i++) {
			map.append("|");
			for(int j=0; j<hitbox.length; j++) {
				map.append(hitbox[j][i]);
				map.append("|");
			}
			map.append("\n");
		}
		return map.toString();
	}

	/**
	 * Retourne la hitbox du plateau.
	 * @return Un tableau de caractères
	 */
	public char[][] getHitbox() {
		return this.hitbox;
	}
	
	/**
	 * Retourne la taille de la hitbox du plateau.
	 * @return Une taille entière
	 */
	public int getTaille() {
		return this.taille;
	}

	/**
	 * Récupère le symbole d'une case vide.
	 * @return Le caractère d'une case vide
	 */
	public static char getCaseVide() {
		return Plateau.CASE_VIDE;
	}

	public int getLongueur() {
		return LONGUEUR;
	}

	public int getLargeur() {
		return LARGEUR;
	}
	
	
	
}
