package moteur;

/**
 * Énumeration définissant un système cardinal.
 * @author Evan
 * @version b02
 * 
 */
public enum Sens {
	
	HAUT(0,-1), BAS(0,1), GAUCHE(-1,0), DROITE(1,0);
	private int x;
	private int y;

	/**
	 * Constructeur générique
	 * @param x Abscisse
	 * @param y Ordonnée
	 */
	private Sens(int x, int y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * Getter générique de l'abscisse
	 * @return Une abscisse
	 */
	public int getX() {
		return x;
	}

	/**
	 * Getter générique de l'ordonnée
	 * @return Une ordonnée
	 */
	public int getY() {
		return y;
	}
	
}
