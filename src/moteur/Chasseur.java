package moteur;

/**
 * Extension de Entite pour les spécificités du Chasseur.
 * @author Evan
 * @version b01
 *
 */ 
public class Chasseur extends Entite{

	public static char symbole = 'C';
	
	/**
	 * Place un Chasseur au hasard sur une hitbox donné.
	 * @param hitbox La hitbox de réference
	 * @param xy Une coordonée
	 */
	public Chasseur(char[][] hitbox, int[] xy) {
		super(hitbox, symbole, '0', xy[0], xy[1]);
	}
}
