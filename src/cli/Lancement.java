package cli;

import java.util.Scanner;

import moteur.Entite;
import moteur.Monstre;
import moteur.Plateau;
import moteur.Sens;

/**
 * Classe lanceant le jeu.
 *  
 * @author Evan,Jonathan,Yohan
 * @version b04
 */
public class Lancement {

	/**
	 * Méthode principale executant le déroulement d'une partie.
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		IO.titleScreen();
		Plateau champ1 = new Plateau(7, 6);
		while(true) {
			IO.clearScreen();
			tour(champ1.monstre, champ1, sc);
			IO.clearScreen();
			tour(champ1.chasseur, champ1, sc);
			
		}
	}
	
	/**
	 * Méthode executant le déroulement d'un tour.
	 * @param joueurActuel Le joueur qui à la main dans ce tour
	 * @param pl Le plateau de la partie
	 */
	public static void tour(Entite joueurActuel, Plateau pl, Scanner sc){
			IO.showMap(joueurActuel, pl);
			IO.showHud(joueurActuel, pl.getTaille());
			Sens s = IO.getSens(sc);
			//DEBUG: System.out.println("" + champ1.chasseur.estDeplacable(s, champ1.getHitbox());
			if(joueurActuel.estDeplacable(s, pl.getHitbox())) {
				if(joueurActuel.estEnCollision(s, pl.getHitbox())) {
					IO.gameOverScreen("CHASSEUR");
					sc.close();
					System.exit(0);
				}else {
					joueurActuel.deplacement(s, pl.getHitbox());
					if(joueurActuel instanceof Monstre && joueurActuel.estVisitee()) {
						((Monstre) joueurActuel).casesVisitees += 1;
					}
				}
			}
			if(joueurActuel instanceof Monstre && ((Monstre) joueurActuel).casesVisitees == pl.getTaille()) {
				IO.gameOverScreen("MONSTRE");
				sc.close();
				System.exit(0);
			}
	}
}
