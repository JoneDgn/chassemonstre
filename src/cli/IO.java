package cli;

import java.util.Scanner;

import moteur.Entite;
import moteur.Monstre;
import moteur.Plateau;
import moteur.Sens;

/**
 * Classe gérant toutes les entrées et sorties utilisateur du jeu dans un terminal.
 *  
 * @author Evan
 * @version b03
 *
 */

public class IO {

	/**
	 * Affiche l'écran titre.
	 * ATTENTION: Ne fontionne que dans un CLI supportant les séquences d'échappement, comme par exemple xterm, xfce4-terminal...
	 */
	public static void titleScreen() {
		for(int i=232; i<256; i++) {
			clearScreen();
			System.out.println("\033[38;5;"+ i +"m");
			System.out.println("    _  _                       _                                     ");
			System.out.println("   FJ  L]    _ ___             L]                                    ");
			System.out.println("  J |  | L  J '__ J            | L  F __ J   J |  | L                ");
			System.out.println("  | |  | |  | |__| |           | | | _____J  | |  | |                ");
			System.out.println("  F L__J J  F L  J J      .--__J J F L___--. F L__J J                ");
			System.out.println(" J\\______/FJ__L  J__L     J\\_____/J\\______/FJ\\____,__L   J__L        ");
			System.out.println("  J______F |__L  J__|      J_____/ J______F  J____,__F   |__|        ");
			System.out.println("\033[39m");
			wait(125);
		}
		clearScreen();
		System.out.println("          Projet de S2.");
		wait(1000);
	}
	
	/**
	 * Affiche un écran de fin de partie.
	 * ATTENTION: Ne fontionne que dans un CLI supportant les séquences d'échappement, comme par exemple xterm, xfce4-terminal...
	 * @param gagnant Le gagnant (e.g. CHASSEUR, MONSTRE)
	 */
	public static void gameOverScreen(String gagnant) {
		for(int i=16; i<52; i++) {
			clearScreen();
			System.out.println("\033[38;5;"+ i +"m");
			System.out.println("                 GAME OVER                      ");
			System.out.println("            VICTOIRE DU "+ gagnant +"                      ");
			System.out.println("\033[39m");
			wait(150);
		}
	}
	
	/**
	 * Nettoie l'écran du jeu.
	 * ATTENTION: Ne fontionne que dans un CLI supportant les séquences d'échappement, comme par exemple xterm, xfce4-terminal...
	 */
    public static void clearScreen() {  
        System.out.print("\033[H\033[2J\n");  
        System.out.flush();  
    }
    
    /**
     * Arrète l'execution du jeu.
     * @param n Temps en millisecondes
     */
    public static void wait(int n) {
		try {
			Thread.sleep(n);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
    }
    
	/**
	 * Attend une entrée du joueur pour le déplacer.
	 * @return Un Sens
	 */
	public static Sens getSens(Scanner sc) {
		while(true) {
			char c = sc.next().charAt(0);
			switch (Character.toUpperCase(c)){
				case 'Z':
					return Sens.HAUT;
				case 'S':
					return Sens.BAS;
				case 'Q':
					return Sens.GAUCHE;
				case 'D':
					return Sens.DROITE;
				default:
					System.out.println("Mauvaise valeur ! Réessaye.");
			}
		}
	}
	
	/**
	 * Affiche une représentation du plateau sur la sortie standard, pour le joueur donné.
	 * @param joueurActuel Le joueur du tour
	 * @param pl Le plateau du jeu
	 */
	public static void showMap(Entite joueurActuel, Plateau pl) {
		if(joueurActuel instanceof Monstre) {
			System.out.println(pl.getMap());
		}else {
			System.out.println(pl.getMap().replaceAll("" + Monstre.symbole, "" + Plateau.getCaseVide()));
		}
	}
	
	/**
	 * Affiche un texte pour donner des indications de la progression du jeu, et des actions possibles.
	 * @param joueurActuel Le joueur du tour
	 */
	public static void showHud(Entite joueurActuel, int casesTotales) {
		if(joueurActuel instanceof Monstre)System.out.println("Cases visités: " + ((Monstre) joueurActuel).casesVisitees + " / " + casesTotales );
		System.out.println("---------------|------------|Haut  : Z");
		System.out.println("|  Tour du     | Que faire ?|Bas   : S ");
		if(joueurActuel instanceof Monstre) {
			System.out.println("|  Monstre     |            |Gauche: Q");
		}else {
			System.out.println("|  Chasseur    |            |Gauche: Q");
		}
		System.out.println("---------------|------------|Droite: D");
	}
}
