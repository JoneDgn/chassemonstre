package ihm;

import java.io.File;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.effect.InnerShadow;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

/**
 * Classe chargé d'afficher les règles du jeu.
 * @author Evan, Yohan, Jonathan
 *
 */
public class Regle implements EventHandler<MouseEvent> {
	
	File fileURLImgFond = new File("ressource/menu.jpg");
    Image imgFond = new Image(fileURLImgFond.toURI().toString());
	double windowWidth;
	double windowHeight;
	
	int taillePl;
	File fileURLImgJeu;
	
	public Regle(double tailleFenetre, int taillePlateau, File file) {
		windowWidth = tailleFenetre;
		windowHeight = tailleFenetre;
		taillePl = taillePlateau;
		fileURLImgJeu = file;
	}
	
	public void handle(MouseEvent event) {
		InnerShadow IS = new InnerShadow();
		VBox root = new VBox(25);
		
		root.setAlignment(Pos.TOP_CENTER);

		root.setBackground(new Background(
				new BackgroundImage(imgFond,
						BackgroundRepeat.NO_REPEAT,
						BackgroundRepeat.NO_REPEAT,
						BackgroundPosition.CENTER,
						new BackgroundSize(500, 500, false, false, false, true))));
		
		Label regle = new Label("R�gles du jeu");
		regle.setFont(Font.font("Fantasy",FontWeight.BOLD,60));
		regle.setTextFill(Color.HOTPINK);
		regle.setEffect(IS);
		

		
		Label chasseur = new Label(" Chasseur : ");
		chasseur.setFont(Font.font("Fantasy",FontWeight.EXTRA_BOLD,40));
		chasseur.setTextFill(Color.AQUA);
		chasseur.setStyle("-fx-background-color: rgba(100,155,0,0.9);-fx-background-radius:10");
		chasseur.setEffect(IS);
		chasseur.setTranslateY(50);
		
		Label textChasseur = new Label(" Votre objectif est de trouver le monstre"
					+" avant que ce dernier ne parcoure \n toute les cases."
					+ " Pour cela, vous pouvez vous d�placer de case en case. \n"
					+ " Si le monstre est d�j� passer sur celle-ci lors d'un tour pr�c�dent, il vous \n le sera indiqu�. ");
		textChasseur.setFont(Font.font("Fantasy",FontWeight.BOLD,17));
		textChasseur.setStyle("-fx-background-color: rgba(255,255,255,0.9);-fx-background-radius:10");
		textChasseur.setTranslateY(55);
		
		Label monstre = new Label(" Monstre : ");
		monstre.setFont(Font.font("Fantasy",FontWeight.EXTRA_BOLD,40));
		monstre.setTextFill(Color.AQUA);
		monstre.setEffect(IS);
		monstre.setStyle("-fx-background-color: rgba(0,150,255,0.9);-fx-background-radius:10;");
		monstre.setTranslateY(70);
		
		Label textMonstre = new Label(" En tant que monstre, votre objectif est de passer sur toutes les cases sans \n"
				+ " vous faire rep�rer par le chasseur. Vous pouvez vous d�placer en case par \n case, "
				+ "ce qui laisse � chaque tour des traces, indiquant ou vous �tes pass�. \n"
				+ " Vous pouvez �galement voir la position actuelle du chasseur. ");
		textMonstre.setTranslateY(80);
		textMonstre.setFont(Font.font("Fantasy",FontWeight.BOLD,17));
		textMonstre.setStyle("-fx-background-color: rgba(255,255,255,0.9);-fx-background-radius:10;");
		
		
		Button retour = new Button("Retour au Menu Principal");
		retour.setStyle("-fx-background-color: rgba(0,0,0,1);");
		retour.setTextFill(Color.WHITE);
		if(windowWidth == 700) {
			retour.setTranslateY(130);
		} else if (windowWidth == 800) {
			retour.setTranslateY(180);
		} else if (windowWidth == 900) {
			retour.setTranslateY(230);
		}
		retour.setCursor(Cursor.HAND);
		
		
		root.getChildren().addAll(regle, chasseur, textChasseur, monstre, textMonstre, retour );

		
		Stage stage = new Stage();
		Scene scene = new Scene(root, windowWidth, windowHeight);
		stage.setTitle("R�gles du jeu");
		stage.setScene(scene);
		stage.setResizable(false);
		stage.show();
		
		// Relance le Menu Principal et ferme la fenetre de jeu
		retour.setOnMouseClicked(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent event) {
				stage.close();
				PlateauJeu pl = new PlateauJeu(windowWidth, taillePl, fileURLImgJeu);
				try {
					pl.start(new Stage());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
	}
}
