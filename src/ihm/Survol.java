package ihm;

import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

// Permet le changement de couleur et de forme du menu
public class Survol implements EventHandler<MouseEvent> {
	public void handle(MouseEvent event) {
		Label lbl = (Label) event.getSource();
		if(lbl.getText().equals("Jouer")) {
			lbl.setTextFill(Color.LIMEGREEN);
			lbl.setText("> Jouer <");
			lbl.setStyle("-fx-background-color: rgba(255,0,0,0.3);-fx-background-radius:10");
		} else if(lbl.getText().equals("R�gles du jeu")){
			lbl.setTextFill(Color.HOTPINK);
			lbl.setText("> R�gles du jeu <");
			lbl.setStyle("-fx-background-color: rgba(255,0,0,0.3);-fx-background-radius:10");
		} else if(lbl.getText().equals("Options")){
			lbl.setTextFill(Color.ORANGE);
			lbl.setText("> Options <");
			lbl.setStyle("-fx-background-color: rgba(255,0,0,0.3);-fx-background-radius:10");
		} else if(lbl.getText().equals("Quitter")) {
			lbl.setTextFill(Color.BLUE);
			lbl.setText("> Quitter <");
			lbl.setStyle("-fx-background-color: rgba(255,0,0,0.3);-fx-background-radius:10");
		}else if(lbl.getText().equals("Retour au Menu Principal")) {
			lbl.setTextFill(Color.BLUE);
			lbl.setText("> Retour au Menu Principal <");
			lbl.setStyle("-fx-background-color: rgba(255,0,0,0.3);-fx-background-radius:10");
		}
	}
}

