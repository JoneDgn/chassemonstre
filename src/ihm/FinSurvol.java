package ihm;

import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.effect.InnerShadow;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

// Permet au menu de revenir à la normale après le changement de forme
public class FinSurvol implements EventHandler<MouseEvent> {
	public void handle(MouseEvent event) {
		InnerShadow IS = new InnerShadow();
		Label lbl = (Label) event.getSource();
		if(lbl.getText().equals("> Jouer <")) {
			lbl.setText("Jouer");
			lbl.setFont(Font.font("Fantasy",FontWeight.EXTRA_BOLD,40));
			lbl.setStyle("");
		} else if(lbl.getText().equals("> R�gles du jeu <")) {
			lbl.setText("R�gles du jeu");
			lbl.setFont(Font.font("Fantasy",FontWeight.EXTRA_BOLD,40));
			lbl.setStyle("");
		} else if(lbl.getText().equals("> Options <")) {
			lbl.setText("Options");
			lbl.setFont(Font.font("Fantasy",FontWeight.EXTRA_BOLD,40));
			lbl.setStyle("");
		} else if(lbl.getText().equals("> Quitter <")){
			lbl.setText("Quitter");
			lbl.setFont(Font.font("Fantasy",FontWeight.EXTRA_BOLD,40));
			lbl.setStyle("");
		} else if(lbl.getText().equals("> Retour au Menu Principal <")){
			lbl.setText("Retour au Menu Principal");
			lbl.setStyle("-fx-background-color: rgba(255, 255, 255, 0.3);-fx-background-radius:10;");
		}
		
			
		lbl.setTextFill(Color.AQUA);
		lbl.setEffect(IS);
	}
}
