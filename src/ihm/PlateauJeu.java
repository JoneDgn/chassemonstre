package ihm;

/**
 * Classe trÃ¨s trÃ¨s sale contenant tout l'IHM
 * TODO: SÃ©parer tout ce bordel.
 * @author Evan,Jonathan,Yohan
 * @version b01
 */
import java.io.File;
import java.util.Optional;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.effect.InnerShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

import moteur.*;

public class PlateauJeu extends Application{
	// Tour en question et personne devant jouer 
	String tourS;
	Label tour;
	int tourN;
	Label tourNum;
	Label caseMonstre;
	int[][] tourMonstre;
	
	// RÃ©glages gÃ©nÃ©raux
	double windowWidth = 700;
	double windowHeight = 700;
	File fileURLImgFond = new File("ressource/menu.jpg");
    Image imgFond = new Image(fileURLImgFond.toURI().toString());
    
	// CrÃ©ation du plateau de taille 6, 6
	int tx = 6;
	int ty = 6;
	Plateau pl;
	GridPane jeu;
	Stage stageMain;
	File fileURLImgJeu = new File("ressource/fond_1.png");
	
	// Pseudo des 2 joueurs
	String nomC;
	File urlChasseur;
	Image imgChasseur;
	ImageView imgViewChasseur;
	int nbImgChasseur = 1;
	final int NBIMGMAXCHASSEUR = 3;
	String nomM;
	File urlMonstre;
	Image imgMonstre;
	ImageView imgViewMonstre;
	int nbImgMonstre = 1;
	final int NBIMGMAXMONSTRE = 6;
	
	public PlateauJeu(double tailleFenetre, int taillePlateau, File file) {
		windowWidth = tailleFenetre;
		windowHeight = tailleFenetre;
		tx = taillePlateau;
		ty = taillePlateau;
		fileURLImgJeu = file;
	}
	
	public PlateauJeu() {
		super();
	}
	
	/**
	 * MÃ©thode affichant une fenetre de choix des pseudos et des personnages pour pouvoir jouer
	 * @author Evan,Jonathan,Yohan
	 *
	 */
	class ChoixPerso implements EventHandler<MouseEvent> {
		public void handle(MouseEvent event) {
			InnerShadow IS = new InnerShadow();
			VBox root = new VBox();
			HBox hbox = new HBox(180);
			
			Label titre = new Label("Choix du personnage");
			titre.setFont(Font.font("Fantasy",FontWeight.BOLD,50));
			titre.setTextFill(Color.HOTPINK);
			titre.setTranslateY(30);
			titre.setEffect(IS);
			
			TextField nameTxtM = new TextField();
			Label nameM = new Label(" Nom ");
			nameM.setFont(Font.font("Fantasy",FontWeight.BOLD,15));
			nameM.setStyle("-fx-background-color: rgba(255,255,255,0.7);-fx-background-radius:10");
			nameM.setTextFill(Color.RED);
			
			HBox hboxM = new HBox(5);
			hboxM.getChildren().addAll(nameM, nameTxtM);
			hboxM.setAlignment(Pos.CENTER);
			
			TextField nameTxtC = new TextField();
			Label nameC = new Label(" Nom ");
			nameC.setFont(Font.font("Fantasy",FontWeight.BOLD,15));
			nameC.setStyle("-fx-background-color: rgba(255,255,255,0.7);-fx-background-radius:10");
			nameC.setTextFill(Color.BLUE);
			
			HBox hboxC = new HBox(5);
			hboxC.getChildren().addAll(nameC, nameTxtC);
			hboxC.setAlignment(Pos.CENTER);
			
			// Gestion Monstre
			Label monstre = new Label(" Monstre ");
			monstre.setFont(Font.font("Fantasy",FontWeight.BOLD,25));
			monstre.setStyle("-fx-background-color: rgba(255,255,255);-fx-background-radius:5");
			monstre.setTextFill(Color.RED);
			monstre.setEffect(IS);
			monstre.setTranslateX(20);		
			
			VBox vboxM = new VBox(25);
			HBox hboxImgM = new HBox();
			
			Button leftM = new Button("<");
			leftM.setTextFill(Color.RED);
			leftM.setFont(Font.font(20));
			leftM.setCursor(Cursor.HAND);
			leftM.setTranslateY(40);
			leftM.setStyle("-fx-background-color: rgba(0,0,0,0.4);-fx-background-radius:10;");
			leftM.addEventHandler(ActionEvent.ACTION, new Change_Img_Monstre());
			
			Button rightM = new Button(">");
			rightM.setTextFill(Color.RED);
			rightM.setFont(Font.font(20));
			rightM.setCursor(Cursor.HAND);
			rightM.setTranslateY(40);
			rightM.setStyle("-fx-background-color: rgba(0,0,0,0.4);-fx-background-radius:10;");
			rightM.addEventHandler(ActionEvent.ACTION, new Change_Img_Monstre());
			
			urlMonstre = new File("ressource/monstre_"+nbImgMonstre+".png");
	        imgMonstre = new Image(urlMonstre.toURI().toString());
	        imgViewMonstre = new ImageView(imgMonstre);
	        imgViewMonstre.setFitHeight(150);
	        imgViewMonstre.setFitWidth(150);
	        hboxImgM.getChildren().addAll(leftM, imgViewMonstre, rightM);
			vboxM.getChildren().addAll(monstre, hboxM, hboxImgM);
			vboxM.setAlignment(Pos.CENTER);
			
			// Gestion Chasseur
			Label chasseur = new Label(" Chasseur ");
			chasseur.setFont(Font.font("Fantasy",FontWeight.BOLD,25));
			chasseur.setStyle("-fx-background-color: rgba(255,255,255);-fx-background-radius:5");
			chasseur.setTextFill(Color.BLUE);
			chasseur.setEffect(IS);
			chasseur.setTranslateX(-25);
			
			VBox vboxC = new VBox(25);
			vboxC.setTranslateX(40);
			HBox hboxImgC = new HBox();
			
			Button leftC = new Button("<");
			leftC.setTextFill(Color.BLUE);
			leftC.setFont(Font.font(20));
			leftC.setCursor(Cursor.HAND);
			leftC.setTranslateY(40);
			leftC.setStyle("-fx-background-color: rgba(0,0,0,0.4);-fx-background-radius:10;");
			leftC.addEventHandler(ActionEvent.ACTION, new Change_Img_Chasseur());
			
			Button rightC = new Button(">");
			rightC.setTextFill(Color.BLUE);
			rightC.setFont(Font.font(20));
			rightC.setCursor(Cursor.HAND);
			rightC.setTranslateY(40);
			rightC.setStyle("-fx-background-color: rgba(0,0,0,0.4);-fx-background-radius:10;");
			rightC.addEventHandler(ActionEvent.ACTION, new Change_Img_Chasseur());
			
			urlChasseur = new File("ressource/chasseur_"+nbImgChasseur+".png");
	        imgChasseur = new Image(urlChasseur.toURI().toString());
	        imgViewChasseur = new ImageView(imgChasseur);
	        imgViewChasseur.setFitHeight(150);
	        imgViewChasseur.setFitWidth(150);
	        
	        // Gestion bouton valider
	        Button creer = new Button("Démarrer");			
			if(windowWidth == 700) {
				creer.setTranslateY(100);
			} else if (windowWidth == 800) {
				creer.setTranslateY(150);
			} else if (windowWidth == 900) {
				creer.setTranslateY(200);
			}
			creer.setStyle("-fx-background-color: rgba(0,0,0,1);");
			creer.setTextFill(Color.WHITE);
			creer.setCursor(Cursor.HAND);
			
	        // Gestion gÃ©nÃ©rale
	        hboxImgC.getChildren().addAll(leftC, imgViewChasseur, rightC);
	        vboxC.getChildren().addAll(chasseur, hboxC, hboxImgC);
			vboxC.setAlignment(Pos.CENTER);
			
			VBox.setMargin(chasseur, new Insets(200, 0, 0, 100));
			VBox.setMargin(monstre, new Insets(200,0, 0, 0));
			
			hbox.getChildren().addAll(vboxC, vboxM);
			root.getChildren().addAll(titre, hbox, creer);
			root.setAlignment(Pos.TOP_CENTER);
			
			// Affichage de l'image de fond
			root.setBackground(new Background(
					new BackgroundImage(imgFond,
							BackgroundRepeat.NO_REPEAT,
							BackgroundRepeat.NO_REPEAT,
							BackgroundPosition.CENTER,
							new BackgroundSize(500, 500, false, false, false, true))));
			
			Stage stageNew = new Stage();
			Scene scene = new Scene(root, windowWidth, windowHeight);
			stageNew.setScene(scene);
			stageNew.setResizable(false);
			stageNew.setTitle("Choix du personnage");
			stageNew.show();
			
			// Permet de lancer une fenetre de jeu avec commme monstre et comme chasseur
			// les nom ecrits dans les TextField correspondant
			// Cela ferme en meme temps la fenetre de de lancement de jeu
			creer.setOnAction(new EventHandler<ActionEvent>() {
				public void handle(ActionEvent event) {
					if(nameTxtC.getText().toString().isEmpty() || nameTxtM.getText().toString().isEmpty()) {
						Alert dialog = new Alert(AlertType.WARNING);
						dialog.setTitle("Erreur");
						dialog.setHeaderText("Oups...");
						dialog.setContentText("Il manque un ou plusieurs pseudo.");
						dialog.showAndWait();
					} else {
						nomC = nameTxtC.getText().toString();
						nomM = nameTxtM.getText().toString();
						stageNew.close();
						try {
							Jouer();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			});
		}
	}
	
	class Change_Img_Monstre implements EventHandler<ActionEvent> {
		public void handle(ActionEvent event) {
			Button b = (Button) event.getSource();
			if(b.getText().equals(">")) {
				if(nbImgMonstre<NBIMGMAXMONSTRE) {
					nbImgMonstre += 1;
					urlMonstre = new File("ressource/monstre_"+nbImgMonstre+".png");
					imgMonstre = new Image(urlMonstre.toURI().toString());
					imgViewMonstre.setImage(imgMonstre);
				}
			} else {
				if(nbImgMonstre>1) {
					nbImgMonstre -= 1;
					urlMonstre = new File("ressource/monstre_"+nbImgMonstre+".png");
					imgMonstre = new Image(urlMonstre.toURI().toString());
					imgViewMonstre.setImage(imgMonstre);
				}
			}
		}
	}
	
	class Change_Img_Chasseur implements EventHandler<ActionEvent> {
		public void handle(ActionEvent event) {
			Button b = (Button) event.getSource();
			if(b.getText().equals(">")) {
				if(nbImgChasseur<NBIMGMAXCHASSEUR) {
					nbImgChasseur += 1;
					urlChasseur = new File("ressource/chasseur_"+nbImgChasseur+".png");
					imgChasseur = new Image(urlChasseur.toURI().toString());
					imgViewChasseur.setImage(imgChasseur);
				}
			} else {
				if(nbImgChasseur>1) {
					nbImgChasseur -= 1;
					urlChasseur = new File("ressource/chasseur_"+nbImgChasseur+".png");
					imgChasseur = new Image(urlChasseur.toURI().toString());
					imgViewChasseur.setImage(imgChasseur);
				}
			}
		}
	}
	
	/**
	 *  Methode affichant le plateau de jeu lors de l'appui sur le bouton jouer depuis le Menu Principal
	 * @throws Exception
	 */
	public void Jouer() throws Exception {
		pl = new Plateau(tx, ty);
		tourN = 1;
		tourS = "Monstre";
		tourMonstre = new int[pl.getLongueur()][pl.getLargeur()];
		BorderPane root = new BorderPane();
	    Image imgFond = new Image(fileURLImgJeu.toURI().toString());
		root.setBackground(new Background(
				new BackgroundImage(imgFond,
						BackgroundRepeat.NO_REPEAT,
						BackgroundRepeat.NO_REPEAT,
						BackgroundPosition.CENTER,
						new BackgroundSize(500, 500, false, false, false, true))));
		
		// Menu de dÃ©cision en bas de fenetre, permettant de choisir la direction,
		// de quitter ou de revenir au Menu Principal
		GridPane decision = new GridPane();
		
		Button gauche = new Button("←");
		gauche.addEventHandler(ActionEvent.ACTION, new Deplacement());
		gauche.setTextFill(Color.WHITE);
		gauche.setFont(Font.font(20));
		gauche.setCursor(Cursor.HAND);
		gauche.setStyle("-fx-background-color: rgba(0,0,0,0.6);-fx-background-radius:10;");
		
		
		Button droite = new Button("→");
		droite.addEventHandler(ActionEvent.ACTION, new Deplacement());
		droite.setTextFill(Color.WHITE);
		droite.setFont(Font.font(20));
		droite.setCursor(Cursor.HAND);
		droite.setStyle("-fx-background-color: rgba(0,0,0,0.6);-fx-background-radius:10;");
		
		Button haut = new Button("↑");
		haut.addEventHandler(ActionEvent.ACTION, new Deplacement());
		haut.setTextFill(Color.WHITE);
		haut.setFont(Font.font(20));
		haut.setCursor(Cursor.HAND);
		haut.setStyle("-fx-background-color: rgba(0,0,0,0.6);-fx-background-radius:10;");
		
		Button bas = new Button("↓");
		bas.addEventHandler(ActionEvent.ACTION, new Deplacement());
		bas.setTextFill(Color.WHITE);
		bas.setFont(Font.font(20));
		bas.setCursor(Cursor.HAND);
		bas.setStyle("-fx-background-color: rgba(0,0,0,0.6);-fx-background-radius:10;");
		
		Button preced = new Button("Menu Principal");
		preced.setStyle("-fx-background-color: rgba(0,0,0,1);");
		preced.setTextFill(Color.WHITE);
		preced.setCursor(Cursor.HAND);
		
		Button quit = new Button("Quitter");
		quit.setStyle("-fx-background-color: rgba(0,0,0,1);");
		quit.setTextFill(Color.WHITE);
		quit.setCursor(Cursor.HAND);
		
		GridPane.setRowSpan(preced, 2);
		GridPane.setRowSpan(quit, 2);
		GridPane.setMargin(preced, new Insets(0, 70, 0, 0));
		GridPane.setMargin(quit, new Insets(0, 0, 0, 70));
		decision.add(preced, 0, 0);
		decision.add(quit, 4, 0);
		decision.add(gauche, 1, 1);
		decision.addColumn(2, haut, bas);
		decision.add(droite, 3, 1);
		decision.setAlignment(Pos.CENTER);
		decision.setHgap(5);
		decision.setVgap(5);
		
		
		// CrÃ©ation du plateau de jeu et mise en forme afin de prendre le plus de place sur la fenetre
		
        Image imgChasseur = new Image(urlChasseur.toURI().toString());
        ImageView imgViewChasseur = new ImageView(imgChasseur);
        imgViewChasseur.setFitHeight(75);
        imgViewChasseur.setFitWidth(75);
        
        Image imgMonstre = new Image(urlMonstre.toURI().toString());
        ImageView imgViewMonstre = new ImageView(imgMonstre);
        imgViewMonstre.setFitHeight(75);
        imgViewMonstre.setFitWidth(75);

        File urlChemin = new File("ressource/chemin.png"); 
	    Image imgChemin = new Image(urlChemin.toURI().toString());
		
		jeu = new GridPane();
		for(int i=0; i<pl.getLongueur(); i++) {
			for(int j=0; j<pl.getLargeur(); j++) {
				if(pl.getHitbox()[i][j] == Monstre.symbole) {
					jeu.add(imgViewMonstre, i, j);
					tourMonstre[i][j] = tourN;
				} else if(pl.getHitbox()[i][j] == Chasseur.symbole) {
					jeu.add(imgViewChasseur, i, j);
				} else {
					jeu.add(new ImageView(imgChemin), i, j);
				}
			}
		}
		if(tx == 4) {
			jeu.getColumnConstraints().setAll( 
	                new ColumnConstraints(150),
	                new ColumnConstraints(150),
	                new ColumnConstraints(150),
	                new ColumnConstraints(150));
				jeu.getRowConstraints().setAll( 
	                new RowConstraints(150),
	                new RowConstraints(150),
	                new RowConstraints(150),
	                new RowConstraints(150));
		} else if(tx == 6) {
			jeu.getColumnConstraints().setAll( 
                new ColumnConstraints(90),
                new ColumnConstraints(90),
                new ColumnConstraints(90),
                new ColumnConstraints(90),
                new ColumnConstraints(90),
                new ColumnConstraints(90));
			jeu.getRowConstraints().setAll( 
                new RowConstraints(90),
                new RowConstraints(90),
                new RowConstraints(90),
                new RowConstraints(90),
                new RowConstraints(90),
                new RowConstraints(90));
		} else if(tx == 8) {
			jeu.getColumnConstraints().setAll( 
	                new ColumnConstraints(70),
	                new ColumnConstraints(70),
	                new ColumnConstraints(70),
	                new ColumnConstraints(70),
	                new ColumnConstraints(70),
	                new ColumnConstraints(70),
	                new ColumnConstraints(70),
	                new ColumnConstraints(70));
				jeu.getRowConstraints().setAll( 
	                new RowConstraints(70),
	                new RowConstraints(70),
	                new RowConstraints(70),
	                new RowConstraints(70),
	                new RowConstraints(70),
	                new RowConstraints(70),
	                new RowConstraints(70),
	                new RowConstraints(70));
		}
		jeu.setAlignment(Pos.CENTER);
		
		
		
		// Fentre d'affichage du tour en cours et de la personne devant jouer
		HBox top = new HBox();
		tour = new Label("Tour du "+tourS);
		tourNum = new Label("         Tour n°"+tourN);
		caseMonstre = new Label("   Le Monstre était ici au tour : "+tourMonstre[pl.chasseur.x][pl.chasseur.y]);
		top.getChildren().addAll(tour, tourNum, caseMonstre);
		top.setAlignment(Pos.CENTER);
		
		root.setBottom(decision);
		root.setTop(top);
		root.setCenter(jeu);
		
		stageMain = new Stage();
		Scene scene = new Scene(root, windowWidth, windowHeight);
		stageMain.setTitle("Chasse au Monstre");
		stageMain.setScene(scene);
		stageMain.setResizable(false);
		stageMain.show();
		
		// Relance le Menu Principal et ferme la fenetre de jeu
		preced.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				stageMain.close();
				try {
					start(new Stage());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		// Fermer la fenetre de jeu avec depuis le boutton quitter
		quit.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				stageMain.close();
			}
		});
	}
	
	/**
	 *  Permet le dÃ©placement du monstre et du chasseur
	 */
	class Deplacement implements EventHandler<ActionEvent> {
		

		public void handle(ActionEvent event) {
			char croix = 'x';
			boolean chasseurWin = false;
			
			File urlChemin = new File("ressource/chemin.png"); 
		    Image imgChemin = new Image(urlChemin.toURI().toString());
		    
			
			Image imgChasseur = new Image(urlChasseur.toURI().toString());
	        ImageView imgViewChasseur = new ImageView(imgChasseur);
	        imgViewChasseur.setFitHeight(75);
	        imgViewChasseur.setFitWidth(75);
	        
	        Image imgMonstre = new Image(urlMonstre.toURI().toString());
	        ImageView imgViewMonstre = new ImageView(imgMonstre);
	        imgViewMonstre.setFitHeight(75);
	        imgViewMonstre.setFitWidth(75);
	        
	        File urlPas = new File("ressource/pas.png"); 
	        Image imgPas = new Image(urlPas.toURI().toString());
			
			Button b = (Button) event.getSource();
			Sens s;
			switch(b.getText()) {
			case "↓":
				s = Sens.BAS;
				break;
			case "↑":
				s = Sens.HAUT;
				break;
			case "→":
				s = Sens.DROITE;
				break;
			default:
				s = Sens.GAUCHE;
				break;
			}
			if(tourS.equals("Monstre")) {
				if(pl.monstre.estDeplacable(s, pl.getHitbox())) {
					if(pl.monstre.estEnCollision(s, pl.getHitbox())) {
						chasseurWin=true;
					}
					pl.monstre.deplacement(s, pl.getHitbox());
					jeu.getChildren().clear();
					tourS = "Chasseur";
					for(int i=0; i<pl.getLongueur(); i++) {
						for(int j=0; j<pl.getLargeur(); j++) {
							if(pl.getHitbox()[i][j] == Monstre.symbole) {
								jeu.add(new ImageView(imgChemin), i, j);
								tourMonstre[i][j] = tourN;
							} else if(pl.getHitbox()[i][j] == Chasseur.symbole) {
								jeu.add(imgViewChasseur, i, j);
							} else if(pl.getHitbox()[i][j] == croix) {
								jeu.add(new ImageView(imgChemin), i, j);
							} else {
								jeu.add(new ImageView(imgChemin), i, j);
							}
						}
					}
				}
			} else {
				if(pl.chasseur.estDeplacable(s, pl.getHitbox())) {
					if(pl.chasseur.estEnCollision(s, pl.getHitbox())) {
						chasseurWin=true;
					}
					pl.chasseur.deplacement(s, pl.getHitbox());
					jeu.getChildren().clear();
					tourS = "Monstre";
					tourN++;
					for(int i=0; i<pl.getLongueur(); i++) {
						for(int j=0; j<pl.getLargeur(); j++) {
							if(pl.getHitbox()[i][j] == Chasseur.symbole) {
								jeu.add(imgViewChasseur, i, j);
							} else if(pl.getHitbox()[i][j] == Monstre.symbole) {
								jeu.add(imgViewMonstre, i, j);
							} else if(pl.getHitbox()[i][j] == croix) {
								jeu.add(new ImageView(imgPas), i, j);
							} else {
								jeu.add(new ImageView(imgChemin), i, j);
							}
						}
					}
				}
			}
			tourNum.setText("         Tour n°"+tourN);
			tour.setText("Tour du "+tourS);
			caseMonstre.setText("   Le Monstre était ici au tour : "+tourMonstre[pl.chasseur.x][pl.chasseur.y]);
			
			// VÃ©rification de la victoire du Monstre 
			boolean monstreWin = true;
			for(int i = 0; i<pl.getLongueur(); i++) {
				for(int j = 0; j<pl.getLargeur(); j++) {
					if(tourMonstre[i][j]==0) {
						monstreWin = false;
					}
				}
			}
			
			if(monstreWin==true) {
				Vainqueur("Monstre", nomM);
			} else if(chasseurWin==true) {
				Vainqueur("Chasseur", nomC);
			}
		}
	}
	
	public void Vainqueur(String winner, String pseudo) {
		Alert dialog = new Alert(AlertType.INFORMATION);
		dialog.setTitle("Fin de partie");
		dialog.setHeaderText("Bien jouer à vous !!");
		dialog.setContentText(pseudo+" a gagné !!\n" +
							"Il était le "+winner);
		Optional<ButtonType> result = dialog.showAndWait();
		if(!result.isPresent() || result.get() == ButtonType.OK) {
			stageMain.close();
			try {
				start(new Stage());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	class Option implements EventHandler<MouseEvent> {
		public void handle(MouseEvent event) {
			InnerShadow IS = new InnerShadow();
			VBox root = new VBox(20);
			root.setAlignment(Pos.TOP_CENTER);
			
			Label option = new Label("Options");
			option.setFont(Font.font("Fantasy",FontWeight.BOLD,60));
			option.setTextFill(Color.HOTPINK);
			option.setEffect(IS);
			Label taille = new Label("Taille de l'écran : ");
			Label dif = new Label("Changer la difficulté : ");
			
			// Gestion Map
			
	        File urlPlt1 = new File("ressource/fond_1.png");
	        Image imgPlt1 = new Image(urlPlt1.toURI().toString());
	        ImageView imgViewPlt1 = new ImageView(imgPlt1);
	        imgViewPlt1.setFitHeight(75);
	        imgViewPlt1.setFitWidth(75);
			
	        File urlPlt2 = new File("ressource/fond_2.png");
	        Image imgPlt2 = new Image(urlPlt2.toURI().toString());
	        ImageView imgViewPlt2 = new ImageView(imgPlt2);
	        imgViewPlt2.setFitHeight(75);
	        imgViewPlt2.setFitWidth(75);
	        
	        File urlPlt3 = new File("ressource/fond_3.png");
	        Image imgPlt3 = new Image(urlPlt3.toURI().toString());
	        ImageView imgViewPlt3 = new ImageView(imgPlt3);
	        imgViewPlt3.setFitHeight(75);
	        imgViewPlt3.setFitWidth(75);
	        
	        ToggleGroup groupMap = new ToggleGroup();
	        RadioButton r1 = new RadioButton();
	        r1.setToggleGroup(groupMap);
	        r1.setSelected(true);
	        RadioButton r2 = new RadioButton();
	        r2.setToggleGroup(groupMap);
	        RadioButton r3 = new RadioButton();
	        r3.setToggleGroup(groupMap);
	        
	        Label img1 = new Label("Désert");
	        VBox v1 = new VBox(5);
	        v1.getChildren().addAll(imgViewPlt1, img1, r1);
	        v1.setAlignment(Pos.CENTER);
	        Label img2 = new Label("Forêt");
	        VBox v2 = new VBox(5);
	        v2.getChildren().addAll(imgViewPlt2, img2, r2);
	        v2.setAlignment(Pos.CENTER);
	        Label img3 = new Label("Montagne");
	        VBox v3 = new VBox(5);
	        v3.getChildren().addAll(imgViewPlt3, img3, r3);
	        v3.setAlignment(Pos.CENTER);
	        
	        Label map = new Label("Changement de la map : ");
	        HBox lMap = new HBox(25);
	        lMap.setAlignment(Pos.CENTER);
	        lMap.getChildren().addAll(v1, v2, v3);
			VBox vboxMap = new VBox(20);
			vboxMap.getChildren().addAll(map, lMap);
			vboxMap.setAlignment(Pos.CENTER);
			
			// Gestion taille
			ComboBox<Label> lTaille = new ComboBox<Label>();
			Label t1 = new Label("700*700");
			Label t2 = new Label("800*800");
			Label t3 = new Label("900*900");
			lTaille.getItems().addAll(t1, t2, t3);
			
			HBox hboxTaille = new HBox();
			hboxTaille.getChildren().addAll(taille, lTaille);
			hboxTaille.setAlignment(Pos.CENTER);
			
			// Gestion plateau
			ComboBox<Label> lDif = new ComboBox<Label>(); 
			Label d1 = new Label("4x4");
			Label d2 = new Label("6x6"); 
			Label d3 = new Label("8x8");
			lDif.getItems().addAll(d1, d2, d3);
			
			HBox hboxDif = new HBox();
			hboxDif.getChildren().addAll(dif, lDif);
			hboxDif.setAlignment(Pos.CENTER);
			
			Button retour = new Button("Retour au Menu Principal");
			if(windowWidth == 500) {
				retour.setTranslateY(70);
			} else if (windowWidth == 700) {
				retour.setTranslateY(250);
			} else if (windowWidth == 900) {
				retour.setTranslateY(400);
			}
			retour.setCursor(Cursor.HAND);
			retour.setStyle("-fx-background-color: rgba(0,0,0,1);");
			retour.setTextFill(Color.WHITE);
	
			
			root.getChildren().addAll(option, hboxTaille, hboxDif, vboxMap, retour);
		
			root.setBackground(new Background(
					new BackgroundImage(imgFond,
							BackgroundRepeat.NO_REPEAT,
							BackgroundRepeat.NO_REPEAT,
							BackgroundPosition.CENTER,
							new BackgroundSize(500, 500, false, false, false, true))));
			
			Stage stage = new Stage();
			Scene scene = new Scene(root, windowWidth, windowHeight);
			stage.setTitle("Options");
			stage.setScene(scene);
			stage.setResizable(false);
			stage.show();
			
			lDif.setOnAction(new EventHandler<ActionEvent>() {
				public void handle(ActionEvent event) {
					int x = Integer.parseInt(lDif.getValue().getText().substring(0, 1));
					int y = Integer.parseInt(lDif.getValue().getText().substring(2, 3));
					tx = x;
					ty = y;
				}
			});
			
			lTaille.setOnAction(new EventHandler<ActionEvent>() {
				public void handle(ActionEvent event) {
					double w = Integer.parseInt(lTaille.getValue().getText().substring(0, 3));
					double h = Integer.parseInt(lTaille.getValue().getText().substring(4, 7));
					stage.setHeight(h);
					stage.setWidth(w);
					if(w == 700) {
						retour.setTranslateY(250);
					} else if (w == 800) {
						retour.setTranslateY(325);
					} else if (w == 900) {
						retour.setTranslateY(400);
					}
					windowWidth = w;
					windowHeight = h;
				}
			});
			
			// Relance le Menu Principal et ferme la fenetre de jeu 
			retour.setOnMouseClicked(new EventHandler<MouseEvent>() {
				public void handle(MouseEvent event) {
					RadioButton selectedRadioButton = (RadioButton) groupMap.getSelectedToggle();
					if(selectedRadioButton.equals(r1)) {
						fileURLImgJeu = new File("ressource/fond_1.png");
					} else if(selectedRadioButton.equals(r2)) {
						fileURLImgJeu = new File("ressource/fond_2.png");
					} else if(selectedRadioButton.equals(r3)) {
						fileURLImgJeu = new File("ressource/fond_3.png");
					}
					stage.close();
					try {
						start(new Stage());
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
		}
	}

	/** 
	 * MÃ©thode gÃ©rant le Menu Principal
	 */
	public void start(Stage stage) throws Exception {
		InnerShadow IS = new InnerShadow();
		
		VBox root = new VBox(25);
		HBox hboxImage = new HBox(290);
		hboxImage.setAlignment(Pos.CENTER);
		 
		
		Label name = new Label("Chasse au Monstre");
		name.setFont(Font.font("Fantasy",FontWeight.BOLD,60));
		name.setTextFill(Color.LIMEGREEN);
		name.setEffect(IS);
		
		
		Label play = new Label("Jouer");
		play.setFont(Font.font("Fantasy",FontWeight.EXTRA_BOLD,40));
		play.setTextFill(Color.AQUA);
		play.setEffect(IS);
		play.setCursor(Cursor.HAND);
		play.addEventHandler(MouseEvent.MOUSE_CLICKED, new ChoixPerso());
		play.addEventHandler(MouseEvent.MOUSE_ENTERED, new Survol());
		play.addEventHandler(MouseEvent.MOUSE_EXITED, new FinSurvol());
		
		
		Label regle = new Label("Règles du jeu");
		regle.setFont(Font.font("Fantasy",FontWeight.EXTRA_BOLD,40));
		regle.setTextFill(Color.AQUA);
		regle.setEffect(IS);
		regle.setCursor(Cursor.HAND);
		regle.addEventHandler(MouseEvent.MOUSE_CLICKED, new Regle(windowHeight, tx, fileURLImgJeu));
		regle.addEventHandler(MouseEvent.MOUSE_ENTERED, new Survol());
		regle.addEventHandler(MouseEvent.MOUSE_EXITED, new FinSurvol());
		
		
		Label option = new Label("Options");
		option.setFont(Font.font("Fantasy",FontWeight.EXTRA_BOLD,40));
		option.setTextFill(Color.AQUA);
		option.setEffect(IS);
		option.setCursor(Cursor.HAND);
		option.addEventHandler(MouseEvent.MOUSE_CLICKED, new Option());
		option.addEventHandler(MouseEvent.MOUSE_ENTERED, new Survol());
		option.addEventHandler(MouseEvent.MOUSE_EXITED, new FinSurvol());
		
	
		Label quit = new Label("Quitter");
		quit.setFont(Font.font("Fantasy",FontWeight.EXTRA_BOLD,40));
		quit.setTextFill(Color.AQUA);
		quit.setEffect(IS);
		quit.setCursor(Cursor.HAND);play.setCursor(Cursor.HAND);
		quit.addEventHandler(MouseEvent.MOUSE_ENTERED, new Survol());
		quit.addEventHandler(MouseEvent.MOUSE_EXITED, new FinSurvol());
		
		// Affichage de l'image de fond
		root.setBackground(new Background(
				new BackgroundImage(imgFond,
						BackgroundRepeat.NO_REPEAT,
						BackgroundRepeat.NO_REPEAT,
						BackgroundPosition.CENTER,
						new BackgroundSize(500, 500, false, false, false, true))));
		
		File chasseur = new File("ressource/chasseur.gif");
		ImageView imgChasseur = new ImageView(chasseur.toURI().toString());
		
		File monstre = new File("ressource/monstre.png");
		ImageView imgMonstre = new ImageView(monstre.toURI().toString());
		 
		hboxImage.getChildren().addAll(imgChasseur,imgMonstre);
		
		
		root.getChildren().addAll(name, play, regle, option, quit , hboxImage);
		root.setAlignment(Pos.CENTER);
		VBox.setMargin(name, new Insets(10, 10, 25, 10));
		
		try {
		File musique = new File("ressource/musique.wav");
		Media media = new Media(musique.toURI().toString());
		MediaPlayer mp = new MediaPlayer(media);	
		mp.play();
		}catch (Exception e) {
			System.err.println("Erreur de lecture audio.");
		}
		
		Scene scene = new Scene(root, windowWidth, windowHeight);
		stage.setTitle("Le Jeu");
		stage.setScene(scene);
		stage.setResizable(false);
		stage.show();
		
		play.setOnMouseClicked(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent event) {
				stage.close();
			}
		});
		
		regle.setOnMouseClicked(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent event) {
				stage.close();
			}
		});
		
		option.setOnMouseClicked(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent event) {
				stage.close();
			}
		});
		
		// Fermer le Menu Principal depuis le bouton quitter
		quit.setOnMouseClicked(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent event) {
				stage.close();
			}
		});
	}
	
	public static void main(String... args) { 
		Application.launch(args); 
		
	}
}
