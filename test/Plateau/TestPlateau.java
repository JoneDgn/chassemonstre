package Plateau;


import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import moteur.Plateau;

public class TestPlateau {

	Plateau plateau;
	
	@Before 
	public void setUp() throws Exception {
		this.plateau= new Plateau(5, 5);
		plateau.getHitbox()[plateau.monstre.x][plateau.monstre.y]='.';
		plateau.getHitbox()[plateau.chasseur.x][plateau.chasseur.y]='.';
	}

	@Test
	public void test() {
		assertEquals(plateau.getMap(),  "|.|.|.|.|.|\n|.|.|.|.|.|\n|.|.|.|.|.|\n|.|.|.|.|.|\n|.|.|.|.|.|\n");					    	          
	}

}
