package Monstre;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import moteur.Monstre;
import moteur.Plateau;
import moteur.Sens;

public class TestMonstre {
	Plateau plateau;

	@Before 
	public void setUp() throws Exception {
		this.plateau=new Plateau(5, 5);
	}

	@Test
	public void test() {
		//case non explorée
		assertTrue(plateau.monstre.estVisitee());
		//déplacement a droite
		if(plateau.monstre.estDeplacable(Sens.DROITE, plateau.getHitbox())) {
			plateau.monstre.deplacement(Sens.DROITE, plateau.getHitbox());
		}
		//déplacement à gauche
		if(plateau.monstre.estDeplacable(Sens.GAUCHE, plateau.getHitbox())) {
			plateau.monstre.deplacement(Sens.GAUCHE, plateau.getHitbox());
		}
		//case explorée
		assertFalse(plateau.monstre.estVisitee());
		

	}

}
